"use strict";
var express = require("express");
var _ = require("lodash");
var XRegExp = require("xregexp");
var URL = require("url-parse");
var app = express();
var allowCrossDomain = function (req, res, next) {
    res.header("Access-Control-Allow-Origin", "http://account.skill-branch.ru");
    res.header("Access-Control-Allow-Methods", "GET,PUT,POST,DELETE");
    res.header("Access-Control-Allow-Headers", "Content-Type");
    next();
};
app.use(allowCrossDomain);
app.get("/task2A", function (req, res) {
    var a = parseFloat(req.query.a);
    var b = parseFloat(req.query.b);
    a = isNaN(a) ? 0 : a;
    b = isNaN(b) ? 0 : b;
    var sum = a + b;
    res.send(sum.toString());
});
app.get("/task2B", function (req, res) {
    var ERROR = "Invalid fullname";
    if (typeof req.query.fullname === "undefined") {
        console.log("Undefined param");
        res.send(ERROR);
        return;
    }
    var fullname = _.trim(req.query.fullname);
    var pattern = XRegExp("^\\s*([\\p{L}\']+)\\s*([\\p{L}\']+)?\\s*([\\p{L}\']+)?\\s*$", "i");
    if (!pattern.test(fullname)) {
        console.log("Bad format = ", fullname);
        res.send(ERROR);
        return;
    }
    var matches = _.filter(fullname.match(pattern).slice(1), function (match) {
        return typeof match !== "undefined";
    });
    var fio = Array();
    switch (matches.length) {
        case 1:
            fio.push(matches[0].charAt(0).toUpperCase() + matches[0].slice(1).toLowerCase());
            break;
        case 2:
            fio.push(matches[1].charAt(0).toUpperCase() + matches[1].slice(1).toLowerCase());
            fio.push(matches[0].charAt(0).toUpperCase() + ".");
            break;
        case 3:
            fio.push(matches[2].charAt(0).toUpperCase() + matches[2].slice(1).toLowerCase());
            fio.push(matches[0].charAt(0).toUpperCase() + ".");
            fio.push(matches[1].charAt(0).toUpperCase() + ".");
            break;
        default:
            res.send(ERROR);
            return;
    }
    res.send(fio.join(" "));
});
app.get("/task2C", function (req, res) {
    var ERROR = "Invalid username";
    if (typeof req.query.username === "undefined") {
        console.log("Undefined param");
        res.send(ERROR);
        return;
    }
    var username = "@";
    var pattern = XRegExp("^@?[\\p{L}]+$", "i");
    if (req.query.username.match(pattern)) {
        username += _.trim(req.query.username, "@");
    }
    else {
        var url = new URL(req.query.username, undefined, true);
        var hostPattern = XRegExp("[\\p{L}]+\\.[\\p{L}]{2,3}", "i");
        if (hostPattern.test(url.pathname) && url.slashes === false) {
            url.pathname = url.pathname.split("/")[1];
        }
        var pathname = _.trim(url.pathname, "/@").split("/")[0];
        username = username + pathname;
    }
    res.send(username);
});
app.listen(3000, function () {
    console.log("Example app listening on port 3000!");
});
//# sourceMappingURL=index.js.map