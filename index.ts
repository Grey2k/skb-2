// tslint:disable-next-line
/// <reference path="types/url-parse.d.ts" />

import express = require("express");
import _ = require("lodash");
import XRegExp = require("xregexp");
import URL = require("url-parse");

import Request = express.Request;
import Response = express.Response;
import NextFunction = express.NextFunction;

let app = express();

// CORS middleware
const allowCrossDomain = function (req: Request, res: Response, next: NextFunction) {
    res.header("Access-Control-Allow-Origin", "http://account.skill-branch.ru");
    res.header("Access-Control-Allow-Methods", "GET,PUT,POST,DELETE");
    res.header("Access-Control-Allow-Headers", "Content-Type");

    next();
};

app.use(allowCrossDomain);

// routes
app.get("/task2A", function (req: Request, res: Response) {

    let a = parseFloat(req.query.a);
    let b = parseFloat(req.query.b);

    a = isNaN(a) ? 0 : a;
    b = isNaN(b) ? 0 : b;

    let sum = a + b;
    res.send(sum.toString());
});

app.get("/task2B", function (req: Request, res: Response) {

    const ERROR = "Invalid fullname";

    if (typeof req.query.fullname === "undefined") {
        // tslint:disable-next-line
        console.log("Undefined param");
        res.send(ERROR);
        return;
    }

    let fullname = _.trim(req.query.fullname);
    let pattern = XRegExp("^\\s*([\\p{L}\']+)\\s*([\\p{L}\']+)?\\s*([\\p{L}\']+)?\\s*$", "i");

    if (!pattern.test(fullname)) {
        // tslint:disable-next-line
        console.log("Bad format = ", fullname);
        res.send(ERROR);
        return;
    }

    let matches = _.filter(fullname.match(pattern).slice(1), function (match) {
        return typeof match !== "undefined";
    });
    let fio = Array<String>();

    switch (matches.length) {
        case 1:
            fio.push(matches[0].charAt(0).toUpperCase() + matches[0].slice(1).toLowerCase());
            break;
        case 2:
            fio.push(matches[1].charAt(0).toUpperCase() + matches[1].slice(1).toLowerCase());
            fio.push(matches[0].charAt(0).toUpperCase() + ".");
            break;
        case 3:
            fio.push(matches[2].charAt(0).toUpperCase() + matches[2].slice(1).toLowerCase());
            fio.push(matches[0].charAt(0).toUpperCase() + ".");
            fio.push(matches[1].charAt(0).toUpperCase() + ".");
            break;
        default:
            res.send(ERROR);
            return;
    }

    res.send(fio.join(" "));
});

app.get("/task2C", function (req: Request, res: Response) {

    const ERROR = "Invalid username";

    if (typeof req.query.username === "undefined") {
        // tslint:disable-next-line
        console.log("Undefined param");
        res.send(ERROR);
        return;
    }

    let username = "@";
    let pattern = XRegExp("^@?[\\p{L}]+$", "i");

    if (req.query.username.match(pattern)) {
        username += _.trim(req.query.username, "@");
    } else {
        let url = new URL(req.query.username, undefined, true);

        let hostPattern = XRegExp("[\\p{L}]+\\.[\\p{L}]{2,3}", "i");
        if (hostPattern.test(url.pathname) && url.slashes === false) {
            url.pathname = url.pathname.split("/")[1];
        }

        let pathname = _.trim(url.pathname, "/@").split("/")[0];

        username = username + pathname;
    }

    res.send(username);
});

app.listen(3000, function () {
    // tslint:disable-next-line
    console.log("Example app listening on port 3000!");
});
